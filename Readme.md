# THE MODEL FOR NITROGEN AND CARBON IN AGRO-ECOSYSTEMS

[English MONICA documentation](home_en.md)

[Deutsche MONICA Dokumentation](home_de.md)

[MONICA GitHub Repository](https://github.com/zalf-rpm/monica)

[MONICA GitHub Wiki / Install instructions](https://github.com/zalf-rpm/monica/wiki)


![](images/zalf_3z_schwarz_cmyk_300.jpg)<br>

[Leibniz-Zentrum für Agrarlandschafts-forschung (ZALF) e.V.<br>
Eberswalder Str. 84<br>
15374 Müncheberg<br>](http://www.zalf.de/en/Pages/ZALF.aspx)

[Research Platform "Models & Simulation @ ZALF](http://www.zalf.de/en/struktur/fpm/Pages/default.aspx)

[Dr. Claas Nendel](http://www.zalf.de/en/ueber_uns/mitarbeiter/Pages/nendel_c.aspx)