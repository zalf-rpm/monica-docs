# CROP PROCESSES

The crop growth concept of MONICA is based on the calculation of assimilate production from radiation. In this manner, the process of photosynthesis is simplified. The efficiency of carbohydrate production is dependent on temperature. The virtual crop’s development from seed to harvest maturity is determined using the accumulating temperature. For each developmental stage the distribution of carbohydrates within the crop is continuously adjusted.  In early stages root and leaf growth is promoted, while shoot and fruit growth will be increasingly supported at later stages. The distributed carbohydrates will be converted into dry matter biomass and – in the case of leaves – also in leaf area. In turn, leaf area as the main location for photosynthesis finds its way into the calculation of carbohydrate production.

The amount of dry matter assigned to the root is distributed down the soil profile according to an empirical formula. The current rooting depth is depending on soil texture, but also on the crop itself. Further on, root length density limits the root’s ability to take up water and nitrogen.  

From leaf area, the degree of soil coverage is estimated, which in turn determines the fraction of transpiration from total evapotranspiration. Actual evapotranspiration is calculated relative to a cut grass crop, using crop-specific coefficients. The resulting water loss is then – a sufficient supply assumed – taken from the respective soil layers according to the root distribution. In case water supply is insufficient and falls below a specific threshold value crop growth will be reduced. This also applies in the case of nitrogen supply from soil being insufficient for maintaining the N concentration in the crop tissue above a critical level.

At harvest marketable parts of the crop are removed and the remaining residues are allocated to conceptual organic matter pools in the soil. Nitrogen in the crop is then divided between marketable yield and crop residues.

1. [Photosynthesis](photosynthesis_en.md)
2. [Respiration](respiration_en.md)
3. [Assimilate partitioning](assimilate_partitioning_en.md)
4. [Ontogenesis](ontogenesis_en.md)
5. [Root growth](root_growth_en.md)
6. [Leaf area and ground coverage](leaf_area_and_ground_coverage_en.md)
7. [Transpiration](transpiration_en.md)
8. [N uptake](n_uptake_en.md)
9. [N concentration](n_concentration_en.md)
10. [Drought stress](drought_stress_en.md)
11. [Nitrogen deficiency](nitrogen_deficiency_en.md)
12. [Oxygen deficiency](oxygen_deficiency_en.md)
13. [Heat stress](heat_stress_en.md)
14. [Yield](yield_en.md)