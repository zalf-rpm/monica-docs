# Oxygen deficiency

Oxygen deficiency in the rooted soil volume is determined by the ration of the actual and the critical air content in the soil. It is assumed that decreasing oxygen concentration in the remaining air volume leads to maximum deficiency effects after four days.

$`\small \zeta_O = 1- \left( \frac{t_{anox}} {4} \right)  \cdot (1 - \zeta_{O_{max}}) `$

$`\small \zeta_O`$	Reduction factor oxygen stress	 <br>
$`\small t_{anox}`$	Time under oxygen deficiency (max = 4)	$`\small [d] `$<br>
$`\small \zeta_{O_{max}} `$	Maximum oxygen deficit	 <br>

where

$`\small \zeta_{O_{max}} = \left( \frac{\varepsilon}{\varepsilon_{crit}} \right)    \,\,\,\,\,\,\,\,\,\,\,\,\, \varepsilon < \varepsilon_{crit}`$

$`\small \zeta_{O_{max}} `$	Maximum oxygen deficit	 <br>
$`\small \varepsilon`$	Air volume in the soil	$`\small [m^3 m^{-3}] `$<br>
$`\small \varepsilon_{crit}`$	Plant specific critical soil oxygen content	$`\small [m^3 m^{-3}] `$<br>

Oxygen stress reduces root water uptake.
 
## References

* Challinor et al. (2005): Simulation of the impact of high temperature stress on annual crop yields. Agricultural and Forest Meteorology 135, 180 - 189.

* Mirschel, W. & Wenkel, K.-O., 2007. Modelling soil-crop interactions with AGROSIM model family. In: K.C. Kersebaum, J.-M. Hecker, W. Mirschel and M. Wegehenkel (Editors), Modelling water and nutrient dynamics in soil crop systems. Springer, Stuttgart, pp. 59- 74.

* Moriondo et al. (2011): Climate change impact assessment: the role of climate extremes in crop yield simulation. Climatic Change 104 (3-4), 679-701.