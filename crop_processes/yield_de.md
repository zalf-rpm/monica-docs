# Ertrag

Der marktfähige Ertrag einer Kultur zurzeit aus dem simulierten Trockenmasseertrag des Speicherorgans berechnet

$`\small E_{FM} = W_s \cdot HI \cdot DM `$

$`\small E_{FM}`$	Frischmasseertrag	$`\small [kg \, FM \, m^{-2}] `$<br>
$`\small W_s`$	Trockenmasse Speicherorgan	$`\small [kg \, DM \, m^{-2}] `$<br>
$`\small HI`$	Ernteeffizienz	 <br>
$`\small DM`$	Trockenmassegehalt	$`\small [kg \, DM \, kg \, FM^{-1}] `$<br>

Der Rohproteingehalt im Ertrag berechnet sich aus dem Verhältnis zwischen Ertrag und Ernterückständen und einem pflanzenspezifischen Parameter, der die Verteilung von Stickstoff zwischen diesen Kompartimenten bestimmt.

$`\small P_{FM} = \frac{N_A} {W_s + (p_N \cdot W_R)} \cdot \frac{1}{N_p}   `$

$`\small P_{FM} `$	Rohproteingehalt im Trockenmasseertrag	$`\small [kg \, Raw \, protein \, kg \, DM^{-1} ] `$<br>
$`\small N_A`$	N-Gehalt in oberirdischer Trockenmasse	$`\small [kg \, N \, m^{-2}] `$<br>
$`\small W_s`$	Trockenmasse Speicherorgan	$`\small [kg \, DM \, m^{-2}] `$<br>
$`\small p_N`$	N-Verteilungskoeffizient	 <br>
$`\small W_R`$	Trockenmasse oberirdischer Ernterückstände	$`\small [kg \, DM \, m^{-2}] `$<br>
$`\small N_p`$	Mittlerer N-Gehalt des Rohproteins (= 0.16)	$`\small [kg \, N \, kg \, Raw \, protein^{-1}] `$<br>

mit

$`\small p_N = \frac{N_R}{N_Y}`$

$`\small p_N `$	N-Verteilungskoeffizient	 <br>
$`\small N_R`$	Durchschnittliche N-Konzentration in Ernterückständen	$`\small [kg \, N \, kg \, DM^{-1}] `$<br>
$`\small N_Y`$	Durchschnittliche N-Konzentration im Ertrag	$`\small [kg \, N \, kg \, DM^{-1}] `$<br>
 