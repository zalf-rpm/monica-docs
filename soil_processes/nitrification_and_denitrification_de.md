# Nitrifikation und Denitrifikation 

$`\small N = K_{NO} \cdot c_{NH^+_4} \cdot f_N(T) \cdot f_N(\theta)`$

$`\small N `$	Nitrifikationsrate	$`\small [kg \, N \, m^{-3} \, d^{-1}] `$<br>
$`\small K_{NO}`$	Nitrifikationsratenkoeffizient bei Standardbedingungen	$`\small [d^{-1}] `$<br>
$`\small c_{NH^+_4}`$	Konzentration NH4+ im Boden	$`\small [kg \, N \, m^{-3}] `$<br>
$`\small f_N(T)`$	Reduktionsfaktor Temperatur (Abb. )	 <br>
$`\small f_N(\theta) `$	Reduktionsfaktor Bodenfeuchte (Abb. 2)	 <br>
 
![](../images/soil_processes/Nitrification_Fig1.png)<br>
Abbildung 1: $`\small f_N(\theta)`$ – Reduktionsfaktor für den Prozess der Nitrifikation in Abhängigkeit von der Bodenwasserspannung (Abrahamsen and Hansen, 2000).
 
![](../images/soil_processes/Nitrification_Fig2.png)<br>
Abbildung 2: Reduktionsfaktor für die Prozesse der Nitrifikation (fN(T)) und der Denitrifikation (fD(T)) in Abhängigkeit von der Bodentemperatur (Abrahamsen and Hansen, 2000).

$`\small D_{pot} = K_{DO} \cdot \Phi_{CO_2} \cdot f_D(T) `$

$`\small D_{pot} `$	Potentielle Denitrifikationsrate	$`\small [kg \, N \, m^{-3} \, d^{-1}] `$<br>
$`\small K_{DO}`$	Anaerobe Denitrifikationsrate	$`\small [kg \, N \, m^{-3} \, d^{-1}] `$<br>
$`\small \Phi_{CO_2}`$	Mikrobielle CO2-Freisetzungsrate	$`\small [kg \, C \, d^{-1}] `$<br>
$`\small f_D(T)`$	Reduktionsfaktor Temperatur (Abb. 2)	 <br>
 
![](../images/soil_processes/Nitrification_Fig3.png)<br>
Abbildung 3: $`\small f_D(\theta)`$ – Reduktionsfaktor für den Prozess der Denitrifikation in Abhängigkeit vom Befüllungsgrad des Porenraums (Abrahamsen and Hansen, 2000).

$`\small D_{act} = min \begin{cases} D_{pot} f_D(\theta) \\ V_{NO_3} \cdot C_{NO_3}  \end{cases} `$

$`\small D_act`$	Aktuelle Denitrifikationsrate	$`\small [kg \, N \, m^{-3} \, d^{-1}] `$<br>
$`\small D_pot`$	Potentielle Denitrifikationsrate	$`\small [kg \, N \, m^{-3} \, d^{-1}] `$<br>
$`\small f_D(\theta)`$	Reduktionsfaktor Bodenfeuchte (Abb. )	$`\small [d^{-1}] `$<br>
$`\small V_{NO_3}`$	Transportrate NO3-	$`\small [kg \, N \, m^{-3}] `$<br>
$`\small C_{NO_3}`$	Konzentration NO3- im Boden	 <br>

## Literatur

* Abrahamsen, P., Hansen, S., 2000. Daisy: an open soil-crop-atmosphere system model. Environ. Mod. Software 15, 313-330.