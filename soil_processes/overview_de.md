# BODENPROZESSE

MONICA beschreibt Prozesse in Boden und Pflanze sowie Austauschprozesse mit der angrenzenden Hydro- und Atmosphäre. Auf der Basis der eingehenden Wetterdaten berechnet MONICA zunächst die Bodentemperatur für die einzelnen diskreten Schichten des Bodens. Die Bewegung des Wassers im Boden wird nach dem Kapazitätsansatz beschrieben. Dabei wird Wasser, das nicht in der Bodenschicht gespeichert werden kann, in die nächste, darunter befindliche Bodenschicht weitergegeben. Die Speicherkapazität und die Flussrate werden durch die Textur und den Humusgehalt des Bodens bestimmt. Evaporation und die Aufnahme von Wasser durch die Pflanzenwurzel beeinflussen den Wasserhaushalt. In Grundwassernähe kann Wasser kapillar in den Wurzelsaum aufsteigen.

Fließendes Wasser transportiert Nitrat. Es entsteht durch den Umsatz organischer Substanz in Ammonium (Ammonifikation) und den weiteren Umsatz von Ammonium in Nitrat (Nitrifikation). Unter Sauerstoffarmut kann auch eine Reaktion von Nitrat zu Luftstickstoff erfolgen (Denitrifikation), bei der das klimawirksame Gas N2O entsteht. Die dabei beteiligten Mikroorganismen und deren Stoffwechsel, bei dem CO2 freigesetzt wird, werden ebenso simuliert. Bei der Ausbringung von Wirtschaftsdüngern gast NH3 aus. Aber auch Harnstoff, ausgebracht als Pflanzendünger, hydrolysiert im Boden und setzt NH3 frei.

1. [Bodentemperatur](soil_temperature_de.md)
2. [Evaporation](evaporation_de.md)
3. [Bodenfeuchte](soil_moisture_de.md)
4. [Bodenfrost](soil_frost_de.md)
5. [Schnee](snow_de.md)
6. [Umsatz organischer Substanz](organic_matter_turn-over_de.md)
7. [Nitrifikation und Denitrifikation](nitrification_and_denitrification_de.md)
8. [Stofftransport](matter_transport_de.md)