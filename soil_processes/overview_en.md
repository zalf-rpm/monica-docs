# SOIL PROCESSES

MONICA describes processes in the soil–plant system and the energy and matter exchange with the hydro- and atmosphere. Using daily weather data, it calculates the soil temperature for single discrete layers in the soil. The movement of water in the soil is modelled using a capacity approach. This approach assumes that water that cannot be stored in a soil layer will be passed on to the adjacent layer below. The layer’s storage capacity and its percolation rate are governed by its texture and soil organic matter content. Evaporation and water uptake by the root influence the water budget. If groundwater is accessible capillary water can rise into the root zone.

Moving soil water carries nitrates. They originate from organic matter turn-over, appearing first as ammonium (ammonification) which is later turned into nitrates (nitrification). If oxygen is deficient, nitrate can be transformed into atmospheric nitrogen (denitrification), a process during which the greenhouse gas N2O is produced. The microorganisms facilitating these processes and producing CO2 from their metabolism are also simulated. When organic fertilisers are applied, gaseous NH3 is set free. Furthermore, also urea fertiliser hydrolyses in the soil and releases NH3.

1. [Soil temperature](soil_temperature_en.md)
2. [Evaporation](evaporation_en.md)
3. [Soil moisture](soil_moisture_en.md)
4. [Soil frost](soil_frost_en.md)
5. [Snow](snow_en.md)
6. [Organic matter turn-over](organic_matter_turn-over_en.md)
7. [Nitrification and Denitrification](nitrification_and_denitrification_en.md)
8. [Matter transport](matter_transport_en.md)